#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Dump json data for each date on local drive for offline access
"""
import os
from datetime import datetime, timedelta
import requests
import json
from main import get_json


if __name__ == '__main__':
    # fetch data for a set of dates. 
    start_date = datetime.strptime('2018-11-17', '%Y-%m-%d')
    end_date = datetime.today() #strptime('2018-11-12', '%Y-%m-%d')
    date_list = [end_date - timedelta(days=x) for x in range(0, (end_date-start_date).days+1)]
    
    # path to save results
    curr_dir = os.path.abspath(os.path.dirname(__file__))
    output_dir = os.path.join(curr_dir, 'json_data')
    
    # api url
    api_base_url = 'https://api.data.gov.sg/v1/environment/air-temperature'


    for count, date in enumerate(date_list):
	    datestr = date.strftime('%Y-%m-%d')
	    # api url is of the format base_url?date=YYYY-MM-DD
	    # eg - 'https://api.data.gov.sg/v1/environment/air-temperature?date=2018-11-11'
	    url = api_base_url + '?' + 'date=' + datestr
	    json_data = get_json(url)
	    filename = datetime.strftime(date, '%Y-%m-%d') + '.json'
	    with open(os.path.join(output_dir, filename), 'w') as write_file:
	    	json.dump(json_data, write_file)