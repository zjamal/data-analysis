#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Determine if certain regions in Singapore are consistently hotter/cooler 
than other regions
data.gov.sg api provides temperature readings for a set of weather stations.
Data contains
- station names and ids in the key metadata
- api status in the key api_info
- per minute readings for each station in the key items
Sample data is available in the file sample_data.json
"""
import os
from datetime import datetime, timedelta
import requests
import json
import pandas as pd
import pdb


def get_json(url):
    """Fetches the data from the url. 
    Returns json if request is valid
    Raises HTTPError
    """
    try:
        r = requests.get(url)
    except requests.exceptions.RequestException as err: # no special handling for individual errors
        print('Data cannot be retrieved for date:' + date + err)
        
    try:
        json_data = json.loads(r.text)
        if is_valid_json(json_data):
            return json_data
    except ValueError:
        return None

def is_valid_json(json_data):
    try:    
        if (json_data and json_data['metadata'] and json_data['metadata']['stations'] and json_data['items']):
            return True
        else:
            return False
    except KeyError:
        return False
        
def get_station_metadata(json_data):
    """Parses the station id, name, latitutde and logitude from input json
    Returns dict with station id as key
    """
    station_metadata = {s['id']:[s['name'], s['location']['latitude'], \
                    s['location']['longitude']] \
                    for s in json_data
                    }
    return station_metadata

def write_data(df, file):
    """Write dataframe to file"""
    with open(file,'w') as f:
        df.to_csv(f, sep=',')


def main():
    """Fetch data from api for a set of dates
    Get station names. Print station names on a map (TBC with google maps api). Note that station
    list is not constant for all dates
    Calculate daily average for each station
    Plot average for all stations
    Use statistical test to determine if the results are statistically significant
    """

    # define set of dates. 
    start_date = datetime.strptime('2017-11-01', '%Y-%m-%d')
    end_date = datetime.today() #strptime('2018-11-12', '%Y-%m-%d')
    date_list = [end_date - timedelta(days=x) for x in range(0, (end_date-start_date).days+1)]
    
    api_base_url = 'https://api.data.gov.sg/v1/environment/air-temperature'

    # path to save data offline 
    curr_dir = os.path.abspath(os.path.dirname(__file__))
    output_dir = os.path.join(curr_dir, 'json_data')

    # data objects
    station_metadata = {} 
    valid_date_list = [] # valid dates for which data is available
    list_temperature_reading = []
    
    # loop through the date, get the data from api, store average temp for each date/station
    for count, date in enumerate(date_list):
        # api url is of the format base_url?date=YYYY-MM-DD
        # eg - 'https://api.data.gov.sg/v1/environment/air-temperature?date=2018-11-11'
        datestr = date.strftime('%Y-%m-%d')
        url = api_base_url + '?' + 'date=' + datestr
        
        # if offline data exists, load it else retrieve it from api
        filename_to_check = datetime.strftime(date, '%Y-%m-%d') + '.json'
        filepath = os.path.join(output_dir, filename_to_check)
        if os.path.exists(filepath):
            with open(filepath, 'r') as read_file:
                json_data = json.load(read_file)
        else:
            json_data = get_json(url)
            # store the file locally
            with open(filepath, 'w') as write_file:
                json.dump(json_data, write_file, indent=4) #indent 4 spaces for pretty print

        if is_valid_json(json_data): # both keys should not be empty
            # add date as a valid date
            valid_date_list.append(date.strftime('%Y-%m-%d'))
            
            # store station id, name, location (latitude/longitude) from metadata
            # with station id as the key
            station_metadata_by_date = get_station_metadata(json_data['metadata']['stations'] )
            # merge station list to add any new stations present on a given data
            station_metadata = dict(station_metadata, **station_metadata_by_date)

            # store average reading for each station
            # first create a temp dict to store the sum, count and average of the readings for each date
            dict_reading_by_date = {s:[0.0, 0, 0.0] for s in station_metadata_by_date.keys()}

            # loop through the per minute readings, increment the station value and count
            for item in json_data['items']:
                timestamp = item['timestamp']
                for reading in item['readings']:
                    station = reading['station_id']
                    value = float(reading['value'])
                    dict_reading_by_date[station][0] = dict_reading_by_date[station][0] + value # increment value
                    dict_reading_by_date[station][1] = dict_reading_by_date[station][1] + 1  # increment count

            for s in dict_reading_by_date.keys():
                if dict_reading_by_date[s][1] > 0: # reading count should be at least 1
                    dict_reading_by_date[s][2] = dict_reading_by_date[s][0]/dict_reading_by_date[s][1]
                else:
                    dict_reading_by_date[s][2] = -100 # default value if reading is not available

            # extract average temperature column
            dict_avg_daily_temp = {s:dict_reading_by_date[s][2] for s in dict_reading_by_date} # check a better method of doing this
            # append daily average reading to the list
            list_temperature_reading.append(dict_avg_daily_temp)
    # pdb.set_trace()
    # create dataframe with the list of all the readings by date
    df_temperature_reading = pd.DataFrame(list_temperature_reading, index=valid_date_list, columns=station_metadata.keys())    
    df_temperature_reading.fillna(-100) # default value -100 if reading is not available
    df_temperature_reading.columns = [station_metadata[id][0] for id in df_temperature_reading.columns]

    write_data(df_temperature_reading, 'reading_summary.csv')
if __name__ == '__main__':
    main()