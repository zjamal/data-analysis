sg-temperature 
This project is to determine if certain regions in Singapore are consistently hotter/cooler 
than other regions. Dataset is from data.gov.sg (https://data.gov.sg/dataset/realtime-weather-readings) 

Installation
For developers
Clone the source locally:

$ git clone https://bitbucket.org/zjamal/data-analysis/sg-temperature
$ cd sg-temperature

Language
Python 3.6