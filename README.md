This is a master repository containing all data analysis projects. Details on each project are included in project's sub-repository

Here is a quick summary of the existing projects:
sg-temperature : This project is to determine if certain regions in Singapore are consistently hotter/cooler 
				 than other regions. Dataset is from data.gov.sg (https://data.gov.sg/dataset/realtime-weather-readings) 